# Installation
```
ln -s .env.dev .env
```

```
docker-compose up -d
```

```
docker exec -it umk_app chown www-data:www-data storage/ bootstrap/ vendor -R
```

```
docker exec -it umk_app composer install
```

on app docker container replace localhost for azure integration

```
docker exec -it umk_app vim /etc/hosts
```

to something like this

```
#127.0.0.1      localhost
192.168.0.101 localhost

# all these bellow is not required, this is automatically build by docker
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.23.0.3      4276efee15c5
172.28.0.2      4276efee15c5
```

where **192.168.0.101** is your computer's local ip address 
